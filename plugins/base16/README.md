Themix Base16 plugin
====================

[![Code Style](https://github.com/themix-project/themix-plugin-base16/actions/workflows/ci.yml/badge.svg)](https://github.com/themix-project/themix-plugin-base16/actions/workflows/ci.yml) [![Commit Activity](https://img.shields.io/github/commit-activity/y/themix-project/themix-plugin-base16?color=pink&logo=amp&logoColor=pink)](https://github.com/themix-project/themix-plugin-base16/graphs/commit-activity)

## Dependencies

 - grep
 - curl
 - moreutils or parallel
 - git
 - rsync
 - find

## FAQ

User templates could be stored in `~/.config/oomox/base16/templates`.

User themes are stored in `~/.config/oomox/colors/__plugin__base16`.
